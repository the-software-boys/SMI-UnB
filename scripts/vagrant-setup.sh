#!/bin/sh

export DB_NAME=postgres
export DB_USER=postgres
export DB_PASS=password
export DB_SERVICE=db
export DB_PORT=5432

echo "Environment installation is beginning. This may take a few minutes.."
echo "Updating package repositories.."
apt-get update

echo "Installing required packages.."
apt-get -y install git

echo "Installing and upgrading pip.."
apt-get -y install python-setuptools
easy_install -U pip

echo "Installing required packages for NFS file sharing for vagrant.."
apt-get -y install nfs-common

echo "Installing required packages for postgres.."
apt-get -y install postgresql

echo "Installing required packages for python package 'psycopg2'.."
apt-get -y install python-dev python3-dev libpq-dev

echo "Installing virtualenvwrapper from pip.."
pip3 install virtualenvwrapper
pip3 install --upgrade virtualenv

sudo apt-get install -y \
     python3              \
     pkg-config           \
     python-psycopg2      \
     python-matplotlib    \
     python3-cffi         \
     libfreetype6-dev

echo "source /usr/local/bin/virtualenvwrapper.sh" >> ~/.bashrc

echo "Install the virtual environment.."
sudo su - ubuntu /bin/bash -c "source /usr/local/bin/virtualenvwrapper.sh;cd /vagrant/;mkvirtualenv --python=`which python3` site; deactivate;mkvirtualenv --python=`which python2` site-python2; deactivate;"

echo "cd /vagrant" >> /home/vagrant/.bashrc

echo ""
echo "The environment has been installed."
echo ""
echo "You can start the machine by running: vagrant up"
echo "You can ssh to the machine by running: vagrant ssh"
echo "You can stop the machine by running: vagrant halt"
echo "You can delete the machine by running: vagrant destroy"
echo ""
exit 0
