import smi_unb.api.views as api_views

from django.conf.urls import include, url
from django.contrib import admin
from rest_framework import routers

from smi_unb import views

router = routers.DefaultRouter()
router.register(r'building', api_views.BuildingViewSet)
router.register(r'energy_measurement', api_views.EnergyMeasurementsViewSet)
router.register(r'energy_transductor', api_views.EnergyTransductorViewSet)

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls)),
    url(r'^campi/', include('smi_unb.campuses.urls', namespace="campuses")),
    url(r'^painel_controle/$', views.dashboard, name="dashboard"),
    url(r'^reports/', include('smi_unb.report.urls', namespace="report")),
    url(r'^usuarios/', include('smi_unb.users.urls', namespace="users")),
    url(
        r'^api-auth/',
        include('rest_framework.urls', namespace='rest_framework')),
    url(
        r'^autenticacao/',
        include('smi_unb.authentication.urls', namespace="authentication")),
    url(
        r'^minha_conta/',
        include('smi_unb.my_account.urls', namespace="my_account")),
    url(
        r'^predios/',
        include('smi_unb.buildings.urls', namespace="buildings")),
    url(
        r'^recuperar_senha/',
        include('smi_unb.retrieve_password.urls', namespace="retrieve")),
    url(
        r'^transdutor/',
        include('smi_unb.transductor.urls', namespace="transductor")),
]
